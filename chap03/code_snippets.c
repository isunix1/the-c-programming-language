/*
 * =====================================================================================
 *
 *       Filename:  code_snippets.c
 *
 *    Description:  this file stores some of the code snippets in chap03
 *
 *        Version:  1.0
 *        Created:  06/20/2014 19:48:25
 *       Revision:  none
 *       Compiler:  gcc
 *
 *         Author:  YOUR NAME (Steven Sun),
 *   Organization:
 *
 * =====================================================================================
 */
#include <stdlib.h>
#include <stdio.h>

/*shellsort: sort v[0] ... v[n-1] into increasing order */

void shellsort(int v[], int n){
  int gap, i, j, temp;

  for (gap = n / 2; gap > 0; gap /= 2){
    for (i = gap; i < n; i++) {
      for (j = i - gap; j >= 0 && v[j] > v[j + gap ]; j -= gap) {
        temp = v[j];
        v[j] = v[j + gap ];
        v[j + gap] = temp;
     }
    }
  }
}
