/*
 * =====================================================================================
 *
 *       Filename:  5.10.c; also has the name find.c for convenience;
 *
 *    Description:  realize the funcion of "find -x -n pattern", it will print each line that doesn't match the pattern, preceed by it line number
 *
 *        Version:  1.0
 *        Created:  06/20/2014 21:16:36
 *       Revision:  none
 *       Compiler:  gcc
 *
 *         Author:  YOUR NAME (Steven Sun),
 *   Organization:
 *
 * =====================================================================================
 */
#include <stdlib.h>
#include <stdio.h>
#define MAXLINE 1000
#include <sys/stat.h>
#include <sys/types.h>

int getLine(char *line, int max);

/*find: print lines that match pattern from 1st arg*/

int main(){
  char line[MAXLINE];
  long lineno = 0;
  int c, except = 0, number = 0, found = 0;

  while (--argc > 0 && (++argv)[0] == '-'){
    while (c = *++argv[0]){
      switch (c){
        case 'x':
          except = 1;
          break;
        case 'n':
          number = 1;
          break;
        default:
          printf("find: illegal option %c\n", c);
          argc = 0;
          found = -1;
          break;
      }
    }

    if (argc != 1){
      printf("usage: find -x -n pattern\n");
    }

    else{
      while (getLine(line, MAXLINE) > 0){
        lineno++;
        if ((strstr(line, *argv) != NULL) != except) {
          if (number){
            printf("%ld:", lineno);
          }
          printf("%s", line);
          found++;
        }
      }
    }
    return found;
  }
}


int getLine(char s[], int lim){
  int c, i;
  for (i = 0; i < lim - 1 && (c = getchar()) != EOF && c != '\n'; i++) {
    s[i] = c;
  }

  if (c == '\n'){
    s[i] = c;
    ++i;
  }

  s[i] = '\0';
  return i;
}


/*met a lot of undeclared variables errors, have no idea how to fix it.*/
