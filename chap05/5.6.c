/*
 * =====================================================================================
 *
 *       Filename:  5.6.c
 *
 *    Description:  sort a set of text lines into alphabetic order, kind of like the "sort" command
 *
 *        Version:  1.0
 *        Created:  06/20/2014 20:40:08
 *       Revision:  none
 *       Compiler:  gcc
 *
 *         Author:  YOUR NAME (Steven Sun),
 *   Organization:
 *
 * =====================================================================================
 */
/*#include <stdlib.h>*/
#include <stdio.h>

#include <string.h>

#define MAXLINES 5000           /*max #liens to be sorted*/
char *lineptr[MAXLINES];        /*Pointers to text lines*/

int readlines(char *lineptr[], int nlines);
void writelines(char *lineptr[], int nlines);

void qsort(char *lineptr[], int left, int right);

/*sort input lines*/

int main(){
  int nlines;                 /*number of input lines read*/
  if ((nlines = readlines(lineptr, MAXLINES)) >= 0) {
    qsort(lineptr, 0, nlines - 1);
    writelines(lineptr, nlines);
    return 0;
  } else {
    printf ("error: input too big to sort\n");
    return 1;
  }
}

int getLine(char s[], int lim){
  int c, i;
  for (i = 0; i < lim - 1 && (c = getchar()) != EOF && c != '\n'; i++) {
    s[i] = c;
  }

  if (c == '\n'){
    s[i] = c;
    ++i;
  }

  s[i] = '\0';
  return i;
}
#define MAXLEN 1000         /*max length of any input line*/

int getLine(char *, int);
char *alloc(int);

/*readlines: read input lines*/

int readlines(char *lineptr[], int maxlines){
  int len, nlines;
  char *p, line[MAXLEN];

  nlines = 0;
  while ((len = getLine(line, MAXLEN)) > 0){
    if (nlines >= maxlines || (p = alloc(len)) == NULL){
      return -1;
    }

    else {
      line[len - 1] = '\0';         /*delete newline*/
      strcpy(p, line);
      lineptr[nlines++] = p;
    }
  return nlines;
  }
}

/*writelines: write output lines*/

void writelines(char *lineptr[], int nlines){
  int i;
  for (i = 0; i < nlines; i++) {
    printf("%s\n", lineptr[i]);
  }
}


/*the program has the following problem
 *5.6.c:83:1: warning: control may reach end of non-void function [-Wreturn-type]
}
^
1 warning generated.
Undefined symbols for architecture x86_64:
  "_alloc", referenced from:
      _readlines in 5-37d6c0.o
ld: symbol(s) not found for architecture x86_64
clang: error: linker command failed with exit code 1 (use -v to see invocation)
 * */
/*The problem is I have no idea how this happened, Although it is clear it has something to do with the "alloc"*/

