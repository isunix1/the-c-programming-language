/*
 * =====================================================================================
 *
 *       Filename:  1.5.4.c
 *
 *    Description:  word counting, every time the program encounters the first character of a word, it counts one more word.
 *
 *        Version:  1.0
 *        Created:  06/20/2014 15:52:51
 *       Revision:  none
 *       Compiler:  gcc
 *
 *         Author:  YOUR NAME (),
 *   Organization:
 *
 * =====================================================================================
 */
#include <stdlib.h>
#include <stdio.h>

#define IN 1   /*inside a word*/
#define OUT 0   /*outside a word*/
/*it does not matter IN is 1 or 0 here*/

int main(){
  int c, nl, nw, nc, state;
  state = OUT;
  nl = nw = nc = 0;
  while((c = getchar()) != EOF){
    ++nc;
    if (c == '\n'){
      ++nl;
    }

    if (c == ' ' || c == '\n' || c == '\t'){
      state = OUT;
    }
    else if (state == OUT){
      state = IN;
      ++nw;
    }
  }
  printf ("%d %d %d\n", nl, nw, nc);
}
