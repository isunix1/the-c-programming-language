#!/usr/bin/env perl

use strict;
use warnings;

my $binary = "0110100000";
#pow(2,8) + pow(2,7) + pow(2,5)  = 16 * 16 + 128 + 32 = 256 + 160 = 416
my $num = bin2dec($binary);

#binary to decimal
sub bin2dec {
  return unpack("N", pack("B32", substr("0" x 32 . shift, -32)));
}

#decimal to binary
sub dec2bin {
  my $str = unpack("B32", pack("N", shift));
  $str =~ s/^0+(?=\d)//;   ##other wise you will get leading zeros
  return $str;
}

print $num;
