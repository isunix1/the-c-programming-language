/*
 * =====================================================================================
 *
 *       Filename:  1.6.c
 *
 *    Description:  count the number of occurrences of each digit, of white space chars(tab, blank, newline) and of all other chars.
 *
 *        Version:  1.0
 *        Created:  06/20/2014 16:49:29
 *       Revision:  none
 *       Compiler:  gcc
 *
 *         Author:  YOUR NAME (),
 *   Organization:
 *
 * =====================================================================================
 */
#include <stdlib.h>
#include <stdio.h>

int main(){
  int c, i, nwhite, nother;
  int ndigit[10];  //declares ndigit to be an array of ten integers

  nwhite = nother = 0;

  for (i = 0; i < 10; i++){
    ndigit[i] = 0;
    //initial the array to be [0,0,0.....0]
  }

  while ((c = getchar()) != EOF){
    if (c >= '0' && c <= '9') {
      //if we input 123,
      ++ndigit[c - '0'];
    }
    else if (c == ' ' || c == '\n' || c == '\t'){
      ++nwhite;
    }
    else {
      ++nother;
    }
  }

  printf ("digits =");

  for (i = 0; i < 10; ++i){
    printf(" %d", ndigit[i]);
  }
  printf(", white space = %d, other = %d\n", nwhite, nother);
}
