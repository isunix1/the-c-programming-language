/*
 * =====================================================================================
 *
 *       Filename:  1.9.c
 *
 *    Description:  reads a set of text lines and prints the longest;
 *
 *        Version:  1.0
 *        Created:  06/20/2014 18:06:31
 *       Revision:  none
 *       Compiler:  gcc
 *
 *         Author:  YOUR NAME (Steven Sun),
 *   Organization:
 *
 * =====================================================================================
 */
#include <stdlib.h>
#include <stdio.h>

#define MAXLINE 100   /*maximum input line size*/

//first declare the two funcs, since we will use them soon after;

int getLine(char line[], int maxline);
void copy(char to[], char from[]);

/*print longest input line*/

int main(){
  int len;        /*current line length*/
  int max;        /*maximum length seen so far*/
  char line[MAXLINE];       /*current input line*/
  char longest[MAXLINE];    /*longest line saved here*/

  max = 0;
  while ((len = getLine(line, MAXLINE)) > 0){
    if (len > max){
      max = len;
      copy(longest, line);
    }
  }

  if (max > 0){       /*there was a line*/
    printf("%s", longest);
  }
  return 0;
}

/*getLine: read a line into s, return length*/

int getLine(char s[], int lim){
  int c, i;
  for (i = 0; i < lim - 1 && (c = getchar()) != EOF && c != '\n'; i++) {
    s[i] = c;
  }

  if (c == '\n'){
    s[i] = c;
    ++i;
  }

  s[i] = '\0';
  return i;
}

/*copy: copy 'from'* into 'to'; assume to is big enough */
void copy(char to[], char from[]){
  int i;
  i = 0;

  while ((to[i] = from[i]) != '\0') {
    ++i;
  }
}

