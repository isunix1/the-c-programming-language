/*
 * =====================================================================================
 *
 *       Filename:  1.5.2_1.c
 *
 *    Description: character count, first version;
 *
 *        Version:  1.0
 *        Created:  06/20/2014 15:30:28
 *       Revision:  none
 *       Compiler:  gcc
 *
 *         Author:  YOUR NAME (),
 *   Organization:
 *
 * =====================================================================================
 */
/*#include <stdlib.h>*/
#include <stdio.h>

int main(){
  long nc;
  nc = 0;
  while (getchar() != EOF){
    ++nc;
  }
  printf("%ld\n", nc);
}
//ld means long integer;
