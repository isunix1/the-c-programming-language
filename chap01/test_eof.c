/*
 * =====================================================================================
 *
 *       Filename:  test_eof.c
 *
 *    Description:  test the use of eof;
 *
 *        Version:  1.0
 *        Created:  06/20/2014 13:54:20
 *       Revision:  none
 *       Compiler:  gcc
 *
 *         Author:  YOUR NAME (Steven Sun),
 *   Organization:
 *
 * =====================================================================================
 */

#include <stdio.h>
int main() {
  char c;
  c = EOF;
  printf ("EOF is: %c\n", c);
}

