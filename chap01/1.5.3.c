/*
 * =====================================================================================
 *
 *       Filename:  1.5.3.c
 *
 *    Description:  count lines in input
 *
 *        Version:  1.0
 *        Created:  06/20/2014 15:45:09
 *       Revision:  none
 *       Compiler:  gcc
 *
 *         Author:  YOUR NAME (),
 *   Organization:
 *
 * =====================================================================================
 */
#include <stdlib.h>
#include <stdio.h>

int main(){
  int c, nl;

  nl = 0;
  while ((c = getchar()) != EOF){
    if (c == '\n'){
      ++nl;
    }
  }
  printf("%d\n", nl);
}
