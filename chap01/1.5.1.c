/*
 * =====================================================================================
 *
 *       Filename:  1.5.1.c
 *
 *    Description:  file copy, copy input to output, input is from the command line, and output will also just be displayed on the screen immediately
 *
 *        Version:  1.0
 *        Created:  06/20/2014 13:54:20
 *       Revision:  none
 *       Compiler:  gcc
 *
 *         Author:  YOUR NAME (Steven Sun),
 *   Organization:
 *
 * =====================================================================================
 */
/*#include <stdlib.h>*/
#include <stdio.h>
int main() {
  int c;
  /*c = getchar();*/
  while (c != EOF) {
    putchar(c);
    c = getchar();
  }
}

