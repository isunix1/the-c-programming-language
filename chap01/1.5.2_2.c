/*
 * =====================================================================================
 *
 *       Filename:  1.5.2_2.c
 *
 *    Description:  char counting version2
 *
 *        Version:  1.0
 *        Created:  06/20/2014 15:37:46
 *       Revision:  none
 *       Compiler:  gcc
 *
 *         Author:  YOUR NAME (),
 *   Organization:
 *
 * =====================================================================================
 */
#include <stdlib.h>
#include <stdio.h>

int main(){
  double nc;
  for (nc = 0; getchar() != EOF; ++nc){
  }
  printf("%.0f\n", nc);
}

/*
 as we can the for loop is empty, we can actually just write is as the following:

  for (nc = 0; getchar() != EOF; ++nc);

 */
