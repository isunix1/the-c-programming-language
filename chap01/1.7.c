/*
 * =====================================================================================
 *
 *       Filename:  1.7.c
 *
 *    Description:  to see how to use function
 *
 *        Version:  1.0
 *        Created:  06/20/2014 18:00:03
 *       Revision:  none
 *       Compiler:  gcc
 *
 *         Author:  YOUR NAME (),
 *   Organization:
 *
 * =====================================================================================
 */
#include <stdlib.h>
#include <stdio.h>

int power(int m, int n);

int main(){
  int i;
  for (i = 0; i < 10; ++i){
    printf("%d  %d  %d\n", i, power(2, i), power(-3, i));
  }
  return 0;
}

int power(int base, int n){
  int i, p;
  p = 1;
  for (i = 0; i < n; i++) {
    p = p * base;
  }
  return p;
}
