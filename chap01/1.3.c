/*
 * =====================================================================================
 *
 *       Filename:  1.3.c
 *
 *    Description:  print Fahrenheit-Celsius table
 *
 *        Version:  1.0
 *        Created:  06/20/2014 15:16:07
 *       Revision:  none
 *       Compiler:  gcc
 *
 *         Author:  YOUR NAME (Steven Sun),
 *   Organization:
 *
 * =====================================================================================
 */
#include <stdlib.h>
#include <stdio.h>

int main(){
  int fahr;
  for (fahr = 0; fahr <= 300; fahr = fahr + 20){
    printf ("%3d %6.1f\n", fahr, (5.0/9.0) * (fahr - 32));
  }
}
